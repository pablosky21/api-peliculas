const { movieSchema, paramsSchema } = require('../midlewares/validar-campos');
const PeliculaSchema = require('../models/peliculas');
const callExternalApi = require('../services/movieService');


const createMovie = async (ctx) => {





    try {
        const url = `${encodeURI(process.env.URL_BASE)}${encodeURI(ctx.params.id)}&apikey=${encodeURI(process.env.SECRET_KEY)}&y=${encodeURI(ctx.query.year)}`;

        const api = await callExternalApi(url);
        const { error, value } = await paramsSchema.validate(ctx.params);
        if (error) {
            ctx.status = 400;
            ctx.body = (`Validation error: ${error.details.map(x => x.message).join(', ')}`);
            console.log(error);
        } else {
            let movie = await PeliculaSchema.findOne({ title: api.Title });
            if (movie) {

                ctx.status = 200;
                ctx.body = { movie };
            } else {
                let result = api.hasOwnProperty('Title');
                if (result) {
                    let movie = new PeliculaSchema({
                        title: api.Title,
                        year: api.Year,
                        released: api.Released,
                        genre: api.Genre,
                        director: api.Director,
                        actors: api.Actors,
                        plot: api.Plot,
                        ratings: api.Ratings


                    })
                    await movie.save()
                    ctx.status = 201;
                    ctx.body = { movie };

                } else {
                    ctx.status = 404;
                    ctx.type = 'json';
                    const msg = {
                        ok: false,
                        msg: 'No hay resultados'
                    }
                    ctx.body = { msg };
                }

            }

        }
    } catch (error) {
        console.log('Error en la funcion: ' + error);
        ctx.status = 500;
        const response = {
            ok: false,
            msg: 'Error generico'
        }
        ctx.body = { response };

    }
}


const allMovies = async (ctx) => {

    var limit = 5;
    let skip = 0
    if (ctx.params.page > 1) {
        skip = ((ctx.params.page - 1) * limit);

    }
    const results = await PeliculaSchema.find().skip(skip).limit(5);
    ctx.status = 200;
    ctx.body = { results };
}

const getAndReeplace = async (ctx) => {

    const { error, value } = await movieSchema.validate(ctx.request.body);

    if (error) {
        ctx.status = 400;
        ctx.body = (`Validation error: ${error.details.map(x => x.message).join(', ')}`);
        console.log(error);
    } else {
        let movie = await PeliculaSchema.findOne({
            title: {
                $regex: new RegExp(ctx.request.body.movie, "i")
            }
        });
        if (movie) {
            var regex = new RegExp('(' + ctx.request.body.find + ')', 'gi');
            const plot = movie.plot.replace(regex, ctx.request.body.replace);
            ctx.status = 200;
            ctx.body = { plot };
        } else {
            ctx.status = 404;
            ctx.type = 'json';
            const response = {
                ok: false,
                msg: 'No hay resultados'
            }
            ctx.body = { response };
        }

    }


}





module.exports = {
    createMovie, allMovies, getAndReeplace
}