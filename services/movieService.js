const fetch = require('node-fetch');

const callExternalApi = async (url) => {
    try {
      const response = await fetch(url);
      let data = await response.json();
      return data;
    } catch (err) {
  
      console.error(err);
    }
  }

module.exports = callExternalApi;