const Joi = require('joi'); 

const movieSchema = Joi.object({
    movie: Joi.string().lowercase().required(),
    find: Joi.string().lowercase().required(),
    replace: Joi.string().lowercase().required()
})

const paramsSchema = Joi.object({
    id: Joi.string().required()
  })

 

module.exports = {
    movieSchema,paramsSchema
}