const koa = require('koa');
const cors = require('@koa/cors');
const bodyparser = require('koa-bodyparser');
const MovieRoute = require('./routes/auth');
const app = new koa();
require('dotenv').config();
const { dbConnection } = require('./database/config');
dbConnection();
app.use(cors());
app.use(bodyparser());
app.use(MovieRoute.routes());

app.listen(process.env.PORT, () => {
    console.log("server alive");
});
