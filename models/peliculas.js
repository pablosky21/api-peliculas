const { Schema, model } = require('mongoose');

const PeliculaSchema = Schema({
    title: {
        type: String
    },
    year: {
        type: String
    },
    released: {
        type: String
    }
    ,
    genre: {
        type: String
    },
    director: {
        type: String
    },
    actors: {
        type: String
    },
    plot: {
        type: String
    },
    ratings: [{
        Source: String,
        Value: String
    }
    ]
});

module.exports = model('Movie', PeliculaSchema);