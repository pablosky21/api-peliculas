const Router = require("@koa/router");

const { createMovie, allMovies,getAndReeplace } = require('../controllers/auth');


const rout = new Router({
    prefix: '/movies'
});
rout.get('/:id', createMovie);
rout.get('/all/:page', allMovies);
rout.post('/getAndReeplace', getAndReeplace);

module.exports = rout;